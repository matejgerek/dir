import ReactDOM from 'react-dom';
import React from 'react';
import MatejComponent from 'components/MatejComponent';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<MatejComponent />, document.getElementById('app'));
});
